//import httpSignature from 'http-signature';
import db from './db';
import config from './config';

/**
 * Create a ActivityStream OrderedCollection from the items from the database.
 *
 * @param details Details for the creation of the collection
 * @param details.collection Collection to get the items from
 * @param details.filter MongoDB to use to filter the items to include
 * @param details.options Additional options for the MongoDB find query
 * @param details.id ID for the collection
 * @param details.context JSON-LD context for the collection (defaults to
 *   the activitystreams context)
 * @param details.page The page of the collection to return
 * @param details.objectMap The function to use to adjust each item before
 *   formatting it for the collection
 *
 * @returns The OrderedCollection JSON-LD Object
 */
export const makePagedCollection = (details) => {
  const context = details.context || 'https://www.w3.org/ns/activitystreams';
  if (!details.page) {
    return db(
      details.collection,
      'countDocuments',
      details.filter,
      details.options
    ).then((count) => {
      if (!count) {
        return {
          '@context': context,
          id: details.id,
          type: 'orderedCollection',
          totalItems: count,
          items: []
        };
      }

      const pages = Math.ceil(count / config.pageSize);

      return {
        '@context': context,
        id: details.id,
        type: 'orderedCollection',
        totalItems: count,
        first: details.id + '?page=1',
        last: details.id + `?page=${pages}`
      };
    });
  }

  const page = Number(details.page);

  if (isNaN(page) || page < 1) {
    return Promise.reject({
      code: 400,
      message: 'Invalid page number given'
    });
  }

  return db(details.collection, 'find', details.filter, {
    ...(details.options || {}),
    limit: config.pageSize,
    skip: config.pageSize * (page - 1)
  })
    .then((cursor) =>
      Promise.all([
        cursor.count(false), // Count items ignoreing skip & limit
        cursor.toArray()
      ])
    )
    .then(([total, objects]) => {
      const pages = Math.ceil(total / config.pageSize);

      if (page > pages) {
        return Promise.reject({
          code: 404,
          message: 'Page not found'
        });
      }

      objects = objects.map(details.objectMap);

      return {
        '@context': context,
        id: details.id + `?page=${page}`,
        type: 'orderedCollectionPage',
        totalItems: total,
        partOf: details.idUrl('c'),
        prev: page > 1 ? details.idUrl('c') + `?page=${page - 1}` : undefined,
        next:
          page < pages ? details.idUrl('c') + `?page=${page + 1}` : undefined,
        orderedItems: objects
      };
    });
};

/**
 * Check if the Accept header value is on of the ActivityStream mime types
 *
 * @param accept The Accept header value
 */
export const acceptsActivityJson = (accept) =>
  accept === 'application/activity+json' ||
  accept === 'application/json' ||
  accept ===
    'application/ld+json; profile="https://www.w3.org/ns/activitystreams"';
