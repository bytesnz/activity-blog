import { accessSync, constants, readFileSync } from 'fs';
import logger from './logger';

const log = logger.getLogger('config');

const {
  BASE_URI,
  MONGODB_OBJECT_COLLECTION,
  MONGODB_ACTIVITY_COLLECTION,
  MONGODB_INCOMING_COLLECTION,
  MONGODB_FOLLOWERS_COLLECTION,
  MONGODB_FOLLOWING_COLLECTION,
  MONGODB_DB,
  MONGODB_URL
} = process.env;

const config = {
  baseUri: BASE_URI || '/',
  db: {
    url: MONGODB_URL || 'mongodb://localhost:27017',
    dbName: MONGODB_DB || 'activity',
    pageCollection: MONGODB_OBJECT_COLLECTION || 'pages',
    objectCollection: MONGODB_OBJECT_COLLECTION || 'objects',
    activityCollection: MONGODB_ACTIVITY_COLLECTION || 'activities',
    incomingCollection: MONGODB_INCOMING_COLLECTION || 'incoming',
    followersCollection: MONGODB_FOLLOWERS_COLLECTION || 'followers',
    followingCollection: MONGODB_FOLLOWING_COLLECTION || 'following'
  },
  me: {},
  profilePage: 'about',
  pageSize: 20,
  menu: [
    {
      href: 'c',
      label: 'home'
    }
  ]
};

try {
  accessSync('config.json', constants.R_OK);

  Object.assign(config, JSON.parse(readFileSync('config.json')));

  if (config.baseUri.lenth > 1) {
    config.baseUri = config.baseUri.replace(/\/$/, '');
  }
} catch (error) {
  log.warn('Error reading config.json:', error.message);
}

export default config;
