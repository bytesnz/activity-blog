import * as path from 'path';
import { watch } from 'chokidar';
import md5 from 'md5';
import aSizeOf from 'image-size';
import * as util from 'util';
import exifr from 'exifr';

const sizeOf = util.promisify(aSizeOf);

const imageFilesGlob = '**/*.@(jpg|jpeg|gif|png|svg|JPG|JPEG|GIF|PNG|SVG)';
const otherFilesGlob = '**/*.@(mp4|MP4)';

// Media database
const media = {};
const galleries = {
  id: md5(''),
  path: '',
  subGalleries: {},
  media: {}
};
const galleriesMap = {
  [md5('')]: galleries
};

const mediaFolder = path.resolve(
  process.cwd(),
  process.env['MEDIA_FOLDER'] || 'media'
);

const addGallery = (folderpath) => {
  const pathParts = folderpath.split('/');

  let gallery = galleries.subGalleries;
  let i = 1;

  while (i <= pathParts.length) {
    const galleryPath = pathParts.slice(0, i).join('/');
    const galleryHash = md5(galleryPath);
    if (gallery[galleryHash]) {
      gallery = gallery[galleryHash].subGalleries;
    } else {
      galleriesMap[galleryHash] = {
        id: galleryHash,
        path: galleryPath,
        subGalleries: {},
        media: {}
      };
      gallery[galleryHash] = galleriesMap[galleryHash];
      gallery = galleriesMap[galleryHash].subGalleries;
    }
    i++;
  }
};

const handleMedia = async (filepath) => {
  const fullPath = path.join(mediaFolder, filepath);
  const hash = md5(filepath);

  const pathParts = path.parse(filepath);

  const galleryHash = md5(pathParts.dir);

  let dimensions, metadata;
  // Get the image dimensions
  try {
    dimensions = await sizeOf(fullPath);
  } catch (error) {
    console.error(
      'Error with getting image dimensions for file',
      fullPath,
      error
    );
  }

  // Get EXIF data
  try {
    metadata = await exifr.parse(fullPath);
  } catch (error) {
    console.info('Error with getting metadata for file', fullPath, error);
  }

  media[hash] = {
    ...dimensions,
    id: hash,
    path: filepath,
    title:
      metadata &&
      ((metadata.title && metadata.title.value) || metadata.caption),
    description:
      metadata &&
      ((metadata.description && metadata.description.value) ||
        metadata.notes ||
        metadata.imageDescription),
    gallery: galleryHash
  };

  // Create the image gallery
  if (pathParts.dir) {
    addGallery(pathParts.dir);
  }

  galleriesMap[galleryHash].media[hash] = media[hash];
};

const handleOtherMedia = async (filepath) => {
  const hash = md5(filepath);

  const pathParts = path.parse(filepath);

  const galleryHash = md5(pathParts.dir);

  media[hash] = {
    id: hash,
    path: filepath,
    gallery: galleryHash
  };

  // Create the image gallery
  if (pathParts.dir) {
    addGallery(pathParts.dir);
  }

  galleriesMap[galleryHash].media[hash] = media[hash];
};

const removeMedia = (filepath) => {
  const hash = md5(filepath);

  if (typeof media[hash] !== 'undefined') {
    delete media[hash];
  }
};

// Scan folder
watch(imageFilesGlob, {
  cwd: mediaFolder
})
  .on('add', (filepath) => {
    handleMedia(filepath);
  })
  .on('change', (filepath) => {
    handleMedia(filepath);
  })
  .on('unlink', (filepath) => {
    removeMedia(filepath);
  });

// Scan folder
watch(otherFilesGlob, {
  cwd: mediaFolder
})
  .on('add', (filepath) => {
    handleOtherMedia(filepath);
  })
  .on('change', (filepath) => {
    handleOtherMedia(filepath);
  })
  .on('unlink', (filepath) => {
    removeMedia(filepath);
  });

export const getGallery = (path) => {
  return galleriesMap[md5(path)] || null;
};
