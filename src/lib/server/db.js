import MongoClient from 'mongodb';
import config from './config';

let conn = new Promise((resolve, reject) => {
  MongoClient.connect(config.db.url, function (err, client) {
    if (err) {
      reject(err);
      return;
    }

    conn = client.db(config.db.dbName);

    resolve();
  });
});

export const db = (collection, operation, ...args) => {
  if (conn instanceof Promise) {
    return conn.then(() => conn.collection(collection)[operation](...args));
  } else {
    const result = conn.collection(collection)[operation](...args);
    if (!(result instanceof Promise)) {
      return Promise.resolve(result);
    }

    return result;
  }
};

export const publishedCollectionFilter = {
  draft: { $ne: true },
  published: { $exists: true },
  public: { $ne: false }
};

export default db;
