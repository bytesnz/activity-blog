import config from './config';
import urlJoin from 'url-join';

export const idUrl = (req, ...parts) =>
  urlJoin(
    ((req.socket && req.socket.secure) ||
    req.headers['x-forwarded-ssl'] === 'on' ||
    req.headers['x-forwarded-scheme'] === 'https' ||
    req.headers['x-forwarded-proto'] === 'https'
      ? 'https'
      : 'http') +
      '://' +
      (req.headers['x-forwarded-host'] || req.headers['host']) +
      config.baseUri,
    ...parts
  );

export const makeIdUrl = (req) => (...parts) => idUrl(req, ...parts);
