import urlJoin from 'url-join';

import Article from '../../schemas/Article';
import Note from '../../schemas/Note';
//import Djv from 'djv';
//
//export const djv = new Djv();
//djv.addSchema('Article', article);
import Ajv from 'ajv';

export const validator = new Ajv();
validator.addSchema(Article, 'Article');
validator.addSchema(Note, 'Note');

export const objectTypes = ['Article', 'Note'];

export const validateObject = (type, value) =>
  !validator.validate(type, value) ? validator.errors.slice() : null;

export const isObject = (object) =>
  typeof object === 'object' && object !== null && !Array.isArray(object);

export const isBadObject = (object) => {
  if (!isObject(object) || !objectTypes.indexOf(object.type) === -1) {
    return true;
  }

  const errors = validateObject(object.type, object);

  if (errors) {
    return errors;
  }

  return null;
};

export const expandTags = (tags, baseUrl) =>
  tags.map((tag) => {
    if (typeof tag === 'string') {
      return {
        id: urlJoin(baseUrl, 'tags', tag),
        name: '#' + tag
      };
    } else {
      return tag;
    }
  });
