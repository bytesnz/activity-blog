import twitchdown, { voidTags } from 'twitchdown';
import hljs from 'highlight.js';
import { getGallery } from '../lib/server/media';

const h = (tag, attributes, children) => {
  if (
    tag === 'a' &&
    attributes.href &&
    attributes.href.match(/^(https?:)?\/\//)
  ) {
    attributes.target = '_blank';
    attributes.rel = (attributes.rel ? attributes.rel + ' ' : '') + 'noopener';
  }

  return (
    '<' +
    tag +
    (attributes
      ? ' ' +
        Object.entries(attributes)
          .filter(([attribute]) => attribute !== 'key')
          .map(([attribute, value]) => attribute + '="' + value + '"')
          .join(' ')
      : '') +
    '>' +
    (voidTags.indexOf(tag) === -1
      ? (children ? children.join('') : '') + '</' + tag + '>'
      : '')
  );
};

const highlight = (code, type) => {
  return (
    '<pre class="' +
    type +
    '"><code>' +
    (type ? hljs.highlight(type, code).value : code) +
    '</code></pre>'
  );
};

const linesGallery = ({ arguments: args }) => {
  const gallery = getGallery(args[0]);

  if (gallery) {
    return (
      '<div class="gallery">' +
      Object.values(gallery.media)
        .map(
          (image) =>
            '<img src="media/' +
            image.path +
            '?gallery"' +
            (image.title ? ' title="' + image.title + '"' : '') +
            (image.description ? ' alt="' + image.description + '"' : '') +
            ' width="' +
            image.width +
            '" height="' +
            image.height +
            '" >'
        )
        .join('') +
      '</div>'
    );
  }

  return '';
};

const mediaUrl = ({ arguments: args }) => `media/${args[0]}`;

export const render = (markdown) => {
  let html = twitchdown(markdown, {
    highlight,
    createElement: h,
    paragraphs: true,
    parseArguments: true,
    customTags: {
      linesGallery,
      assetUrl: mediaUrl
    },
    headingOffset: 2,
    replacePunctuation: true
  }).join('');

  return html;
};
