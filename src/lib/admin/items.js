let subscriptions = [];

let changeStream = null;

const handleChange = (event) => {};

let feed = null;
let fetched = null;

const newInstance = () => ({
  items: feed,
  fetched: fetched,
  expanded: null,
  added: 0,
  updated: [],
  deleted: []
});

const getFeed = (myFetch) => {
  myFetch = myFetch || fetch;

  return myFetch('admin/c.json')
    .then((r) => r.json())
    .then((items) => {
      fetched = new Date();
      feed = items.map((item) => ({
        ...item,
        published: new Date(item.published)
      }));
    });
};

export function preload(slug) {
  const myFetch = (this && this.fetch) || fetch;

  const instance = newInstance();

  let promise;
  if (slug) {
    promise = myFetch(`admin/c/${slug}.json`).then((response) => {
      return response.json().then((data) => {
        if (response.status === 200) {
          instance.expanded = data;
          instance.expanded.published = new Date(instance.expanded.published);
        } else {
          this.error(response.status, data.message);
        }
      });
      console.log('data', data);
    });
  } else if (feed === null) {
    promise = getFeed(myFetch).then(() => {
      instance.items = feed;
      instance.fetched = fetched;
    });
  } else {
    promise = Promise.resolve();
  }

  return promise.then(() => instance);
}

export function subscribe(slug, instance) {
  //TODO const myFetch = (this && this.fetch) || fetch;

  if (!instance) {
    instance = newInstance();
  } else if (instance.items && !feed) {
    feed = instance.items;
    fetched = instance.fetched;
  }

  subscriptions.push(instance);

  if (!Object.prototype.hasOwnProperty.call(instance, 'unsubscribe')) {
    Object.defineProperties(instance, {
      unsubscribe: {
        value: () => {
          const index = subscriptions.indexOf(instance);

          if (index !== -1) {
            subscriptions.splice(index, 1);
          }

          if (!subscriptions.length) {
            changeStream.close();
            changeStream = null;
          }
        }
      },
      reload: { value: (amount) => {} },
      more: { value: (amount) => {} },
      delete: {
        value: (slug) => {
          return fetch(`admin/object/${slug}`, {
            method: 'DELETE'
          }).then((response) => {
            if (response.status >= 200 && response < 300) {
              if (feed) {
                const index = feed.findIndex((object) => object.slug === slug);

                if (index !== -1) {
                  feed.splice(index, 1);
                }

                subscriptions.forEach((subscription) => {
                  if (subscription === instance) {
                    const index = subscription.items.findIndex(
                      (object) => object.slug === slug
                    );

                    if (index !== -1) {
                      subscription.items.splice(index, 1);
                    }

                    if (
                      subscription.expanded &&
                      subscription.expanded.slug === slug
                    ) {
                      subscription.expanded = null;
                    }
                  } else {
                    subscription.deleted.push(slug);
                  }
                });
              }
            } else {
              return Promise.reject(new Error('Unknown response from server'));
            }
          });
        }
      }
    });
  }

  if (instance.feed === null) {
  }

  /* TODO
  if (changeStream === null) {
    const stream = db(objectCollection, 'watch');

    if (stream instanceof Promise) {
      stream.then((s) => {
        changeStream = s;
        changeStream.on('change', handleChange);
      });
    } else {
      changeStream = stream;
      changeStream.on('change', handleChange);
    }
  }
  */

  return instance;
}
