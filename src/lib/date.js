const pad = (number) => (number < 10 ? '0' + number : number);

/**
 * Format a date to be used in a datetime-local input
 * (yyyy-mm-ddTHH:MM)
 *
 * @param {Date|string|number} date Date to format
 *
 * @returns {string} Formatted date
 */
export const toDatetimeLocal = (date) => {
  if (!(date instanceof Date)) {
    date = new Date(date);
  }

  return (
    date.getFullYear() +
    '-' +
    pad(date.getMonth() + 1) +
    '-' +
    pad(date.getDate()) +
    'T' +
    pad(date.getHours()) +
    ':' +
    pad(date.getMinutes())
  );
};
