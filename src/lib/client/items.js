let subscriptions = [];

let changeStream = null;

let feed = null;
let fetched = null;

const newInstance = () => ({
  items: feed,
  fetched: fetched,
  expanded: null,
  added: 0
});

const getFeed = (myFetch) => {
  myFetch = myFetch || fetch;

  return myFetch('c.json')
    .then((r) => r.json())
    .then((items) => {
      fetched = new Date();
      feed = items.map((item) => ({
        ...item,
        published: new Date(item.published)
      }));
    });
};

/**
 * Fetch article or feed
 */
export function preload(slug) {
  const myFetch = (this && this.fetch) || fetch;

  const instance = newInstance();

  let promise;
  if (slug) {
    promise = myFetch(`c/${slug}.json`).then((response) => {
      return response.json().then((data) => {
        if (response.status === 200) {
          instance.expanded = data;
          instance.expanded.published = new Date(instance.expanded.published);
        } else {
          this.error(response.status, data.message);
        }
      });
    });
  } else if (feed === null) {
    promise = getFeed(myFetch).then(() => {
      instance.items = feed;
      instance.fetched = fetched;
    });
  } else {
    promise = Promise.resolve();
  }

  return promise.then(() => instance);
}

/**
 * "Subscribe" to data
 */
export function subscribe(callback, slug, instance) {
  const subscription = {
    callback,
    added: 0,
    updated: [],
    deleted: []
  };

  subscriptions.push(subscription);

  if (!instance) {
    instance = newInstance();
  } else if (process.browser && instance.items && !feed) {
    feed = instance.items;
    fetched = instance.fetched;
  }

  if (!Object.prototype.hasOwnProperty.call(instance, 'unsubscribe')) {
    Object.defineProperties(instance, {
      unsubscribe: {
        value: () => {
          const index = subscriptions.indexOf(subscription);

          if (index !== -1) {
            subscriptions.splice(index, 1);
          }

          if (!subscriptions.length) {
            changeStream.close();
            changeStream = null;
          }
        }
      },
      reload: { value: () => {} },
      more: { value: () => {} }
    });
  }

  if (instance.items === null) {
    if (process.browser) {
      getFeed().then(() => {
        callback({
          ...instance,
          items: feed,
          fetched: fetched
        });
      });
    }
  }

  /* TODO
  if (changeStream === null) {
    const stream = db(objectCollection, 'watch');

    if (stream instanceof Promise) {
      stream.then((s) => {
        changeStream = s;
        changeStream.on('change', handleChange);
      });
    } else {
      changeStream = stream;
      changeStream.on('change', handleChange);
    }
  }
  */

  return instance;
}
