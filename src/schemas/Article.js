export default {
  type: 'object',
  properties: {
    type: {
      const: 'Article'
    },
    slug: {
      type: 'string',
      pattern: '^[a-z0-9-]*$'
    },
    published: {
      type: 'string',
      format: 'date-time'
    },
    updated: {
      type: 'string',
      format: 'date-time'
    },
    deleted: {
      type: 'string',
      format: 'date-time'
    },
    name: {
      type: 'string'
    },
    tags: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    draft: {
      type: 'boolean'
    },
    summary: {
      type: 'string',
      contentType: 'text/markdown'
    },
    content: {
      type: 'string',
      contentType: 'text/markdown'
    }
  },
  required: ['type', 'name', 'content']
};
