export default {
  type: 'object',
  properties: {
    type: {
      const: 'Note'
    },
    published: {
      type: 'string',
      format: 'date-time'
    },
    deleted: {
      type: 'string',
      format: 'date-time'
    },
    tags: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    draft: {
      type: 'boolean'
    },
    content: {
      type: 'string',
      contentType: 'text/markdown'
    }
  },
  required: ['type', 'content']
};
