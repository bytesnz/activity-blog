export default {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    title: {
      type: 'string'
    },
    draft: {
      type: 'boolean'
    },
    public: {
      type: 'boolean'
    },
    content: {
      type: 'string'
    }
  },
  required: ['_id', 'title', 'content'],
  additionalProperties: false
};
