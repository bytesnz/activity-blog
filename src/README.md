# activity-blog <!=package.json version>

A basic blogging system using markdown formatting, [ActivityStreams][] and
[ActivityPub][], written using [Svelte][] and [Sapper][].

## Running the project

### [Docker][]

Two [Docker][] images are [available on Gitlab][images]:

- **registry.gitlab.com/bytesnz/activity-blog** an image containing just the
  blog interface
- **registry.gitlab.com/bytesnz/activity-blog/admin** an image containing both
  the blog interface and the admin interface

As no authentication is currently built into activity-blog, the image with
only the blog interface should be publically accessible.

### Code

However you get the code, you can install dependencies and run the project in
development mode with:

```bash
cd activity-blog
npm install # or yarn
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.

#### Production mode and deployment

To start a production version of your app, run `npm run build && npm start`. This will disable live reloading, and activate the appropriate bundler plugins.

You can deploy your application to any environment that supports Node 10 or above. As an example, to deploy to [Vercel Now](https://vercel.com) when using `sapper export`, run these commands:

```bash
npm install -g vercel
vercel
```

If your app can't be exported to a static site, you can use the [now-sapper](https://github.com/thgh/now-sapper) builder. You can find instructions on how to do so in its [README](https://github.com/thgh/now-sapper#basic-usage).

[activitystreams]: https://activitystrea.ms/
[activitypub]: https://activitypub.rocks/
[svelte]: https://svelte.dev/
[sapper]: https://sapper.svelte.dev/
[images]: https://gitlab.com/bytesnz/activity-blog/container_registry
