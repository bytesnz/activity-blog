import config from '../../lib/server/config';
import { idUrl } from '../../lib/server/url';

export function get(req, res, next) {
  if (!req.query.resource || req.query.resource !== `acct:${config.account}`) {
    res.writeHead(400);
    res.end();
  }

  res.writeHead(200, {
    'Content-Type': 'application/jrd+json'
  });

  res.end(
    JSON.stringify({
      subject: `acct:${config.account}`,
      aliases: [idUrl(req)],
      links: [
        {
          rel: 'http://webfinger.example/rel/profile-page',
          href: idUrl(req, config.profilePage),
          type: 'text/html'
        },
        {
          rel: 'self',
          type: 'application/activity+json',
          href: idUrl(req)
        },
        {
          rel: 'http://ostatus.org/schema/1.0/subscribe',
          template: idUrl(req, `authorize?uri={uri}`)
        }
      ]
    })
  );
}
