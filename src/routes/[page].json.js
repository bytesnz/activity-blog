import config from '../lib/server/config';
import db from '../lib/server/db';
import { render } from '../lib/markdown';
import logger from '../lib/server/logger';

const log = logger.getLogger('page.json');

/**
 * Return page JSON from database
 */
export async function get(req, res) {
  const { page } = req.params;

  return db(
    config.db.pageCollection,
    'findOne',
    { _id: page },
    {
      projection: { _id: 0 }
    }
  ).then(
    (result) => {
      if (result === null) {
        res.writeHead(404, {
          'Content-Type': 'application/json'
        });
        res.end(
          JSON.stringify({
            message: 'Page not found'
          })
        );

        return;
      }

      res.writeHead(200, {
        'Content-Type': 'application/json'
      });

      result.content = render(result.content);
      res.end(JSON.stringify(result));
    },
    (error) => {
      log.error('DB returned an error', error);

      res.writeHead(500, {
        'Content-Type': 'application/json'
      });

      res.end(JSON.stringify(error));
    }
  );
}
