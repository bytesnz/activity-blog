import { db, publishedCollectionFilter } from '../../lib/server/db';
import { render } from '../../lib/markdown';
import config from '../../lib/server/config';
import logger from '../../lib/server/logger';

const log = logger.getLogger('c.json');

const feedize = (item) => {
  if (item.type === 'Article') {
    return {
      type: item.type,
      name: item.name,
      slug: item._id,
      published: item.published,
      tags: item.tags,
      summary: (item.summary && render(item.summary)) || undefined
    };
  }
  return {
    type: item.type,
    name: item.name,
    slug: item._id,
    published: item.published,
    tags: item.tags,
    content: (item.content && render(item.content)) || null
  };
};

let contents = '[]';

const updateFeed = () => {
  return db(config.db.objectCollection, 'find', publishedCollectionFilter, {
    sort: {
      published: -1
    }
  })
    .then((cursor) => cursor.toArray())
    .then(
      (items) => {
        contents = JSON.stringify(items.map(feedize));
      },
      (error) => {
        log.error('Error updating feed:', error);
      }
    );
};

updateFeed();

// Temp update every hour
if (process.env.NODE_ENV !== 'development') {
  setInterval(updateFeed, 3600000);
}

/**
 * Request handler for returning the collection
 */
export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  if (
    process.env.NODE_ENV === 'development' ||
    req.headers['Cache-Control'] === 'no_cache'
  ) {
    return updateFeed().then(() => {
      res.end(contents);
    });
  }

  res.end(contents);
}
