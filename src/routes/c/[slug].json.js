import config from '../../lib/server/config';
import db from '../../lib/server/db';
import { render } from '../../lib/markdown';
import logger from '../../lib/server/logger';

const log = logger.getLogger('objects');

let cache = new Map();

const updateCache = () => {
  return db(
    config.db.objectCollection,
    'find',
    {
      draft: { $ne: true },
      published: { $exists: true },
      type: 'Article'
    },
    {
      sort: {
        published: 1
      }
    }
  )
    .then(
      (cursor) => {
        return cursor.toArray();
      },
      (error) => {
        log.error('Error updating cache:', error);
      }
    )
    .then((items) => {
      items.forEach((item) => {
        cache.set(
          item._id.toString(),
          JSON.stringify({
            type: item.type,
            name: item.name,
            slug: item._id.toString(),
            published: item.published,
            tags: item.tags,
            keywords: item.keywords,
            description: item.description,
            summary: (item.summary && render(item.summary)) || null,
            content: (item.content && render(item.content)) || null
          })
        );
      });
    });
};

updateCache();

// Temp update every hour
if (process.env.NODE_ENV !== 'development') {
  setInterval(updateCache, 3600000);
}

/**
 * Get the object and return as JSON
 */
export async function get(req, res) {
  // the `slug` parameter is available because
  // this file is called [slug].json.js
  const { slug } = req.params;

  if (
    process.env.NODE_ENV === 'development' ||
    req.headers['Cache-Control'] === 'no_cache'
  ) {
    await updateCache();
  }

  if (cache.has(slug)) {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });

    res.end(cache.get(slug));
  } else {
    res.writeHead(404, {
      'Content-Type': 'application/json'
    });

    res.end(
      JSON.stringify({
        message: `Not found`
      })
    );
  }
}
