import { expandTags } from '../../lib/server/utils';
import { publishedCollectionFilter } from '../../lib/server/db';
import {
  acceptsActivityJson,
  makePagedCollection
} from '../../lib/server/activityPub';
import config from '../../lib/server/config';
import { makeIdUrl } from '../../lib/server/url';

/**
 * Respond with ActivityStream JSON-LD when requested
 */
export function get(req, res, next) {
  if (!acceptsActivityJson(req.headers['accept'])) {
    return next();
  }

  const idUrl = makeIdUrl(req);

  return makePagedCollection({
    idUrl,
    filter: publishedCollectionFilter,
    options: {
      sort: {
        published: -1
      }
    },
    page: req.query.page,
    collection: config.db.objectCollection,
    id: idUrl('c'),
    objectMap: (object) => {
      const asObject = {
        type: object.type,
        attributedTo: idUrl(),
        tag: expandTags(object.tags || [], idUrl())
      };

      let skip = ['_id', 'draft', 'tags'];
      if (object.type === 'Article') {
        asObject.url = idUrl('c', object._id);
        skip.push('content');
      }

      const keys = Object.keys(object);

      for (let i = 0; i < keys.length; i++) {
        if (skip.indexOf(keys[i]) !== -1) {
          continue;
        }
        asObject[keys[i]] = object[keys[i]];
      }

      return asObject;
    }
  }).then(
    (object) => {
      res.writeHead(200, {
        'Content-Type': 'application/activity+json'
      });
      res.end(JSON.stringify(object));
      //return next();
    },
    (error) => {
      res.writeHead(error.code, {
        'Content-Type': 'application/json'
      });
      res.end(JSON.stringify(error));
      //return next();
    }
  );
}
