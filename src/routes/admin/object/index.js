import config from '../../../lib/server/config';
import db from '../../../lib/server/db';
import { isBadObject, isObject } from '../../../lib/server/utils';

const newId = () => {
  let id = '';

  for (let i = 0; i < 24; i++) {
    const r = Math.floor(Math.random() * 16);
    id += r < 10 ? r : String.fromCharCode(r + 87);
  }

  return id;
};

/**
 * Create the new object
 */
export async function put(req, res) {
  if (
    req.headers['content-type'] !== 'application/json' ||
    !req.body ||
    !isObject(req.body)
  ) {
    res.statusCode = 400;
    res.end();
    return;
  }

  const errors = isBadObject(req.body);

  if (errors === true) {
    res.statusCode = 400;
    res.end();
    return;
  } else if (errors) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        message: 'Object is not valid',
        errors
      })
    );
    return;
  }

  if (req.body.published) {
    req.body.published = new Date(req.body.published);
  } else {
    req.body.published = new Date();
  }

  if (req.body.slug) {
    const conflict = await db(config.db.objectCollection, 'findOne', {
      _id: req.body.slug
    });

    if (conflict) {
      res.statusCode = 409;
      res.end();
      return;
    }

    req.body._id = req.body.slug;
    delete req.body.slug;
  } else {
    req.body._id = newId();
  }

  const result = await db(config.db.objectCollection, 'insertOne', req.body);

  res.statusCode = 201;
  res.setHeader('Content-Type', 'application/json');
  res.end(
    JSON.stringify({
      slug: result.insertedId
    })
  );
}
