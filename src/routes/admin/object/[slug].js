import config from '../../../lib/server/config';
import db from '../../../lib/server/db';
import { isBadObject, isObject } from '../../../lib/server/utils';

/**
 * Get the object
 */
export async function get(req, res) {
  const result = await db(config.db.objectCollection, 'findOne', {
    _id: req.params.slug
  });

  if (!result) {
    res.statusCode = 404;
    res.end();
    return;
  }

  result.slug = result._id;
  delete result._id;

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(result));
}

/**
 * Delete the object
 */
export async function del(req, res) {
  const result = await db(config.db.objectCollection, 'findOne', {
    _id: req.params.slug
  });

  if (!result) {
    res.statusCode = 404;
    res.end();
    return;
  }

  await db(config.db.objectCollection, 'deleteOne', {
    _id: req.params.slug
  });

  res.statusCode = 200;
  res.end();
}

/**
 * Update the object
 */
export async function patch(req, res) {
  if (
    req.headers['content-type'] !== 'application/json' ||
    !req.body ||
    !isObject(req.body)
  ) {
    res.statusCode = 400;
    res.end();
    return;
  }

  const errors = isBadObject(req.body);

  if (errors === true) {
    res.statusCode = 400;
    res.end();
    return;
  } else if (errors) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        message: 'Object is not valid',
        errors
      })
    );
    return;
  }

  if (req.body.published) {
    req.body.published = new Date(req.body.published);
  } else {
    req.body.published = new Date();
  }

  // Find the existing object
  const original = await db(config.db.objectCollection, 'findOne', {
    _id: req.params.slug
  });

  if (!original) {
    res.statusCode = 404;
    res.end();
    return;
  }

  req.body._id = req.body.slug;
  delete req.body.slug;

  // Check for conflict if involving move
  if (req.body._id !== req.params.slug) {
    const conflict = await db(config.db.objectCollection, 'findOne', {
      _id: req.body._id
    });

    if (conflict) {
      res.statusCode = 409;
      res.end();
      return;
    }

    // Insert the new one
    const result = await db(config.db.objectCollection, 'insertOne', req.body);

    // Tombstone the original
    if (req.body.slug !== req.params.slug) {
      await db(
        config.db.objectCollection,
        'replaceOne',
        {
          _id: req.params.slug
        },
        {
          _id: req.params.slug,
          type: 'Tombstone',
          formerType: original.type,
          deleted: new Date()
        }
      );
    }

    res.statusCode = 201;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        slug: result.insertedId
      })
    );
  } else {
    const result = await db(
      config.db.objectCollection,
      'replaceOne',
      {
        _id: req.params.slug
      },
      req.body
    );

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        slug: result.insertedId
      })
    );
  }
}
