import config from '../../../lib/server/config';
import db from '../../../lib/server/db';
import { render } from '../../../lib/markdown';
import logger from '../../../lib/server/logger';

const log = logger.getLogger('admin:objects');

const feedize = (item) => {
  if (item.type === 'Article') {
    return {
      type: item.type,
      name: item.name,
      slug: item._id,
      published: item.published,
      draft: item.draft,
      tags: item.tags,
      summary: (item.summary && render(item.summary)) || undefined
    };
  }
  return {
    type: item.type,
    name: item.name,
    slug: item._id,
    published: item.published,
    draft: item.draft,
    tags: item.tags,
    content: (item.content && render(item.content)) || null
  };
};

let contents = '[]';

const updateFeed = () => {
  return db(
    config.db.objectCollection,
    'find',
    {},
    {
      sort: {
        published: -1
      }
    }
  )
    .then(
      (cursor) => {
        return cursor.toArray();
      },
      (error) => {
        log.error('Error updating feed:', error);
      }
    )
    .then((items) => {
      contents = JSON.stringify(items.map(feedize));
    });
};

updateFeed();

/**
 * Return the objects array JSON
 */
export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  return updateFeed().then(() => {
    res.end(contents);
  });
}
