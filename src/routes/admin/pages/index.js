import config from '../../../lib/server/config';
import db from '../../../lib/server/db';
import { isObject, validator } from '../../../lib/server/utils';
import Page from '../../../schemas/Page';

/**
 * Create a new page
 */
export async function put(req, res) {
  if (
    req.headers['content-type'] !== 'application/json' ||
    !req.body ||
    !isObject(req.body)
  ) {
    res.statusCode = 400;
    res.end();
    return;
  }

  let errors = null;
  if (!validator.validate(Page, req.body)) {
    errors = validator.errors.slice;
  }

  if (errors) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        message: 'Page is not valid',
        errors
      })
    );
    return;
  }

  const conflict = await db(config.db.pageCollection, 'findOne', {
    _id: req.body._id
  });

  if (conflict) {
    res.statusCode = 409;
    res.end();
    return;
  }

  const result = await db(config.db.pageCollection, 'insertOne', req.body);

  res.statusCode = 201;
  res.setHeader('Content-Type', 'application/json');
  res.end(
    JSON.stringify({
      slug: result.insertedId
    })
  );
}
