import config from '../../../lib/server/config';
import db from '../../../lib/server/db';
import { isObject, validator } from '../../../lib/server/utils';
import Page from '../../../schemas/Page';

/**
 * Delete a page
 */
export async function del(req, res) {
  const result = await db(config.db.pageCollection, 'findOne', {
    _id: req.params._id
  });

  if (!result) {
    res.statusCode = 404;
    res.end();
    return;
  }

  await db(config.db.pageCollection, 'deleteOne', {
    _id: req.params._id
  });

  res.statusCode = 200;
  res.end();
}

/**
 * Update a page
 */
export async function patch(req, res) {
  if (
    req.headers['content-type'] !== 'application/json' ||
    !req.body ||
    !isObject(req.body)
  ) {
    res.statusCode = 400;
    res.end();
    return;
  }

  let errors = null;
  if (!validator.validate(Page, req.body)) {
    errors = validator.errors.slice;
  }

  if (errors) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        message: 'Page is not valid',
        errors
      })
    );
    return;
  }

  // Find the existing object
  const original = await db(config.db.pageCollection, 'findOne', {
    _id: req.params.page
  });

  if (!original) {
    res.statusCode = 404;
    res.end();
    return;
  }

  // Check for conflict if involving move
  if (req.body._id !== req.params.page) {
    const conflict = await db(config.db.pageCollection, 'findOne', {
      _id: req.body._id
    });

    if (conflict) {
      res.statusCode = 409;
      res.end();
      return;
    }

    // Insert the new one
    const result = await db(config.db.pageCollection, 'insertOne', req.body);

    // TODO Move Activity

    res.statusCode = 201;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        slug: result.insertedId
      })
    );
  } else {
    const result = await db(
      config.db.pageCollection,
      'replaceOne',
      {
        _id: req.params.page
      },
      req.body
    );

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(
      JSON.stringify({
        slug: result.insertedId
      })
    );
  }
}
