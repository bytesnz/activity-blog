import db from '../../../lib/server/db';
import config from '../../../lib/server/config';
import logger from '../../../lib/server/logger';

const log = logger.getLogger('pages.json');

/**
 * Return page JSON from database
 */
export async function get(req, res) {
  return db(
    config.db.pageCollection,
    'find',
    {},
    {
      projection: { _id: 1, title: 1 }
    }
  )
    .then((cursor) => cursor.toArray())
    .then(
      (pages) => {
        res.writeHead(200, {
          'Content-Type': 'application/json'
        });

        res.end(JSON.stringify(pages));
      },
      (error) => {
        log.error('DB returned an error', error);

        res.writeHead(500, {
          'Content-Type': 'application/json'
        });

        res.end(JSON.stringify(error));
      }
    );
}
