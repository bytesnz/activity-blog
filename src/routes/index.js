import config from '../lib/server/config';
import { acceptsActivityJson } from '../lib/server/activityPub';
import { makeIdUrl } from '../lib/server/url';

/**
 * Respond with ActivityStream JSON-LD when requested
 */
export function get(req, res, next) {
  if (!acceptsActivityJson(req.headers['accept'])) {
    return next();
  }

  const idUrl = makeIdUrl(req);

  const object = {
    '@context': [
      'https://www.w3.org/ns/activitystreams',
      {
        manuallyApprovesFollowers: 'as:manuallyApprovesFollowers'
      }
    ],
    ...config.me,
    type: 'Person',
    url: idUrl(),
    id: idUrl(),
    inbox: idUrl('inbox'),
    outbox: idUrl('activity'),
    followers: idUrl('followers'),
    following: idUrl('following')
  };

  res.writeHead(200, {
    'Content-Type': 'application/activity+json'
  });

  res.end(JSON.stringify(object));
  //return next();
}
