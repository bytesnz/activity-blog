import {
  acceptsActivityJson,
  makePagedCollection
} from '../lib/server/activityPub';
import config from '../lib/server/config';
import { makeIdUrl } from '../lib/server/url';

/**
 * Respond with ActivityStream JSON-LD when requested
 */
export function get(req, res, next) {
  if (!acceptsActivityJson(req.headers['accept'])) {
    return next();
  }

  const idUrl = makeIdUrl(req);

  return makePagedCollection({
    config,
    idUrl,
    page: req.query.page,
    collection: config.db.followingCollection,
    id: idUrl('following'),
    objectMap: (object) => object._id
  }).then(
    (object) => {
      res.writeHead(200, {
        'Content-Type': 'application/activity+json'
      });
      res.end(JSON.stringify(object));
      //return next();
    },
    (error) => {
      res.writeHead(error.conde, {
        'Content-Type': 'application/json'
      });
      res.end(JSON.stringify(error));
      //return next();
    }
  );
}
