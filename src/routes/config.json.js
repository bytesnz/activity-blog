import config from '../lib/server/config';

/**
 * Return the menu items
 */
export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });
  res.end(
    JSON.stringify({
      menu: config.menu,
      contactLinks: config.contactLinks
    })
  );
}
