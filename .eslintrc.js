module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
  ],
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module'
  },
  env: {
    es6: true,
    node: true
  },
  plugins: ['svelte3'],
  rules: {
    'no-console': [1, { allow: ['warn', 'error'] }],
    'no-debugger': 1,
    'no-warning-comments': [
      1,
      { terms: ['xxx', 'todo', 'fixme'], location: 'anywhere' }
    ],
    'require-jsdoc': 1
  },
  overrides: [
    {
      files: ['*.svelte'],
      env: {
        es6: true,
        browser: true
      },
      processor: 'svelte3/svelte3'
    },
    {
      files: ['src/lib/client/*.js', 'src/lib/admin/*.js', 'src/lib/*.js' ],
      env: {
        browser: true
      }
    },
    {
      files: ['utils/*.js'],
      extends: ['plugin:node/recommended'],
    },
    {
      files: ['src/server.js', 'src/lib/server/*.js'],
      rules: {
        'no-console': [1, { allow: ['warn', 'error', 'log'] }]
      }
    }
  ]
};
