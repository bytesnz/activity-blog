cp-ignored -f .prodignore . prod
cd prod
yarn
yarn run build
docker build -t ${1:=activity-blog} -f docker/Dockerfile .
cd ..
rm -rf prod
