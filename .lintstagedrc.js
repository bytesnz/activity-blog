module.exports = {
  '*.{css,md,json}': ['prettier --write', 'git add'],
  '*.{js,svelte}': [
    'prettier --write',
    'eslint -c .eslintrc.commit.js --fix',
    'git add'
  ]
};
