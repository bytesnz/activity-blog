# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2022-11-25

### Added

- global.css to docker container for default style
- theme.css to HTML to allow for the customisation of the inbuilt theme

### Changed

- Updated to latest version of twitchdown allowing for replacement of --, ---
  and ... for endash, emdash and ellipsis respectively

### Fixed

- Published date not being set correctly in article editor

## [0.3.0] - 2022-10-11

### Added

- ARIA labels to the main navigation and the article collection list

### Changed

- Arrangement of data in articles (data and tags) so it is better for screen
  readers
- Ensured the feed isn't loaded on the server so if on an article page, the
  initial page only contains the article
- Ensure the feed isn't cached on the server

## [0.2.0] - 2022-08-28

### Removed

- Article close link and changing of article title to collection link as
  closing an article does not work (it scrolls incorrectly)

## [0.1.9] - 2022-08-28

### Added

- keywords and description metadata tag fields
- Implemented tag collection filtering
- Allow collection cache override with `no-cache` header

## [0.1.8] - 2022-06-16

### Changed

- Displaying only dates for object times
- Disabled the editor addClosing feature
- Enabled spellchecking on the editor
- Show other objects without opacity once scrolled 1/3 of innerHeight instead
  of full height

## [0.1.7] - 2022-01-27

### Fixed

- Fixed formatting of the published date for the data input

## [0.1.6] - 2022-01-27

### Fixed

- Ensure published date is formatted as a ISO8601 datestring

## [0.1.5] - 2021-02-26

### Fixed

- Heavily reduced docker image size by moving yarn cache during build to
  temporary filesystem /dev/sdm
  (see https://github.com/yarnpkg/rfcs/pull/53#issuecomment-399678507)
- Made contact icons larger

### Added

- Added dark theme using [`prefers-color-scheme`](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)
  media query

## [0.1.4] - 2021-02-24

### Fixed

- Fixed continue editing link in admin section

## [0.1.3] - 2021-01-10

### Fixed

- Fixed hostname not being set in JSON-LD objects correctly when behind a
  proxy

## [0.1.1] - 2020-11-28

### Fixed

- Padding on pages
- Scrolling on collection

## [0.1.0] - 2020-11-27

### Added

- Basic [ActivityPub](https://www.w3.org/TR/activitypub/) and
  [Activity Streams](https://www.w3.org/TR/activitystreams-core/) functionality
  (GET requests only)
- Menu configuration through config
- Page handling and editing

## [0.0.1] - 2020-11-26

Initial version with basic functionality:

- object collection
- Article and Note writing using an admin interface

[0.2.0]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.9...v0.2.0
[0.1.9]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.8...v0.1.9
[0.1.8]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.1...v0.1.3
[0.1.1]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/activity-blog/-/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/bytesnz/activity-blog/tree/v0.0.1
